import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { chatMessageSelector } from "../slices/chatMessage";
import { userSelector } from "../slices/user";
import style from "../Styles/chatPage.module.css";

const MessagesBox = (props) => {
  const { all_messages } = useSelector(chatMessageSelector);
  const { current_user } = useSelector(userSelector);

  useEffect(() => {
    props.setChatMsg(all_messages);
  }, [all_messages]);

  return (
    <>
      <div
        className={`${style.messages_displayer}`}
        style={props.istyping ? { height: "362px" } : {}}
      >
        {props.chatMsg?.length > 0 ? (
          props.chatMsg?.map((msg) => {
            return msg.contentType === "image" ? (
              <div className={`${style.alignerImg}`} key={msg._id}>
                <div
                  className={
                    msg.sender._id === current_user._id
                      ? `${style.rightMessage_containerImg}`
                      : `${style.leftMessage_containerImg}`
                  }
                  key={msg._id}
                >
                  <a href={msg.content}>
                    <img
                      src={msg.content}
                      alt="chatpic"
                      className={`${style.chatImg}`}
                    />
                  </a>
                </div>
              </div>
            ) : msg.contentType === "video" ? (
              <div className={`${style.alignerImg}`} key={msg._id}>
                <div
                  className={
                    msg.sender._id === current_user._id
                      ? `${style.rightMessage_containerImg}`
                      : `${style.leftMessage_containerImg}`
                  }
                  key={msg._id}
                >
                  <video
                    src={msg.content}
                    controls
                    className={`${style.chatVideo}`}
                  />
                </div>
              </div>
            ) : (
              <div className={`${style.aligner}`} key={msg._id}>
                <div
                  className={
                    msg.sender._id === current_user._id
                      ? `${style.rightMessage_container}`
                      : `${style.leftMessage_container}`
                  }
                  key={msg._id}
                >
                  <p
                    className={
                      msg.sender._id === current_user.id
                        ? `${style.rightMessage}`
                        : `${style.leftMessage}`
                    }
                  >
                    {msg.content}
                  </p>
                </div>
              </div>
            );
          })
        ) : (
          <></>
        )}
      </div>
    </>
  );
};

export default MessagesBox;
