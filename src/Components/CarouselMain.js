import React, { useEffect, useState } from "react";
import { Carousel } from "antd";
import axios from "axios";

const CarouselMain = () => {
  const [carouselImage, setCarouselImage] = useState();
  const contentStyle = {
    height: "20rem",
    color: "#fff",
    textAlign: "center",
  };
  const imagesStyle = {
    height: "30rem",
    width: "100%",
  };

  const getCarouselImages = () => {
    var config = {
      method: "get",
      url: "http://localhost:8080/carousel_images",
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        setCarouselImage(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    getCarouselImages();
  }, []);

  return (
    <>
      <Carousel autoplay speed={800}>
        {carouselImage?.map((image) => {
          return (
            <div style={contentStyle} key={image._id}>
              <img src={image.img} alt={image.name} style={imagesStyle} />
            </div>
          );
        })}
      </Carousel>
    </>
  );
};

export default CarouselMain;
