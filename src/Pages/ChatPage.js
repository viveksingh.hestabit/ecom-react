import React from "react";
import ChatBox from "../Components/ChatBox";
import Navbar from "../Components/Navbar";

const ChatPage = () => {
  return (
    <>
      <Navbar />
      <ChatBox />
    </>
  );
};

export default ChatPage;
