import React, { useState, useEffect } from "react";
import { Alert, Button, Snackbar } from "@mui/material";
import axios from "axios";
import { useParams } from "react-router-dom";
import style from "../Styles/singleProduct.module.css";
import StarRateIcon from "@mui/icons-material/StarRate";
import StarHalfIcon from "@mui/icons-material/StarHalf";
import StarBorderIcon from "@mui/icons-material/StarBorder";
import { useDispatch } from "react-redux";
import { loadCurrentUser } from "../slices/user";
import LocalShippingOutlinedIcon from "@mui/icons-material/LocalShippingOutlined";
import CategoryOutlinedIcon from "@mui/icons-material/CategoryOutlined";
import SettingsSuggestOutlinedIcon from "@mui/icons-material/SettingsSuggestOutlined";

const SingleProduct = () => {
  const { prodId } = useParams();
  const [selectedImg, setSelectedImg] = useState();
  const [product, setProduct] = useState();
  const [logError, setLogError] = useState();
  const [open, setOpen] = useState(false);
  const [successMsg, setSuccessMsg] = useState();
  const [successOpen, setSuccessOpen] = useState(false);
  const dispatch = useDispatch();

  const getProduct = () => {
    var config = {
      method: "get",
      url: `http://localhost:8080/product/${prodId}`,
    };
    axios(config)
      .then(function (res) {
        // console.log(res.data);
        setProduct(res.data);
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  const addToCart = () => {
    var data = JSON.stringify({
      user_ID: {
        _id: localStorage.getItem("loginId"),
      },
      product: {
        product_id: prodId,
        price: product.price,
        name: product.name,
        description: product.description,
        img_url: product.prod_img_url,
        quantity: 1,
        stock: product.stock,
      },
    });
    var config = {
      method: "post",
      // for express
      url: "http://localhost:8080/add_to_cart",
      // for moleculer
      // url: "http://localhost:5000/api/user/product/add_to_cart",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };
    axios(config)
      .then(function (res) {
        // console.log(JSON.stringify(response.data));
        if (res.data.success) {
          dispatch(loadCurrentUser());
          setSuccessOpen(true);
          setSuccessMsg(res.data.message);
        } else {
          setLogError(res.data.message);
          setOpen(true);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    getProduct();
  }, []);

  return (
    <>
      <div className={`${style.content_carrier}`}>
        <div className={`${style.container}`}>
          <div className={`${style.image_holder}`}>
            <img
              src={selectedImg ? selectedImg.url : product?.prod_img_url}
              alt="product"
              className={`${style.product_image}`}
            />
            <div className={`${style.facilities_container}`}>
              <div className={`${style.facility}`}>
                <LocalShippingOutlinedIcon style={{ color: "#fff" }} />
                <p className={`${style.facility_desc}`}>
                  Fast, Easy and no contact delivery
                </p>
              </div>
              <div className={`${style.facility}`}>
                <CategoryOutlinedIcon style={{ color: "#fff" }} />
                <p className={`${style.facility_desc}`}>7 days easy return</p>
              </div>
              <div className={`${style.facility}`}>
                <SettingsSuggestOutlinedIcon style={{ color: "#fff" }} />
                <p className={`${style.facility_desc}`}>Free installation</p>
              </div>
            </div>
          </div>
          <div className={`${style.info_container}`}>
            <h1 className={`${style.heading}`}>{product?.name}</h1>
            <div className={`${style.product_ratings}`}>
              <div>
                <StarRateIcon style={{ color: "rgb(235, 235, 235)" }} />
                <StarRateIcon style={{ color: "rgb(235, 235, 235)" }} />
                <StarHalfIcon style={{ color: "rgb(235, 235, 235)" }} />
                <StarBorderIcon style={{ color: "rgb(235, 235, 235)" }} />
                <StarBorderIcon style={{ color: "rgb(235, 235, 235)" }} />
              </div>
              <p className={`${style.rating_user_number}`}>(122)</p>
            </div>
            <p className={`${style.description}`}>
              {product?.description} Lorem ipsum dolor sit amet consectetur
              adipisicing elit. Ipsam quis quasi ipsum natus pariatur! In sint
              nulla numquam, dolores sapiente voluptas neque nemo fugit. Ab
              perspiciatis omnis repellat dolore praesentium! Repellendus
              maiores praesentium esse aut hic rem quibusdam placeat aperiam
              atque exercitationem. Fugiat voluptas, quisquam soluta ipsam iure,
              commodi in quis culpa nesciunt ratione unde veniam assumenda.
              Repellendus, eius enim.
            </p>
            <div className={`${style.color_options}`}>
              {product?.colorOptions.map((prod) => {
                return (
                  <div className={`${style.img_name_holder}`} key={prod._id}>
                    <img
                      src={prod.url}
                      alt="product color"
                      className={`${style.img_product}`}
                      onClick={() => setSelectedImg(prod)}
                    />
                    <p className={`${style.color_product}`}>{prod.color}</p>
                  </div>
                );
              })}
            </div>
            <h2 className={`${style.price}`}>&#8377; {product?.price}</h2>
            {logError ? (
              <>
                <Snackbar
                  open={open}
                  autoHideDuration={2000}
                  onClose={() => setOpen(false)}
                >
                  <Alert severity="error">{logError}!</Alert>
                </Snackbar>
              </>
            ) : (
              <></>
            )}
            <Button
              variant="contained"
              style={{
                color: "rgb(235, 235, 235)",
                backgroundColor: "rgb(7, 31, 75)",
              }}
              onClick={addToCart}
            >
              Add To Cart
            </Button>
          </div>
        </div>
      </div>
      {successMsg ? (
        <Snackbar
          open={successOpen}
          autoHideDuration={2000}
          onClose={() => setSuccessOpen(false)}
        >
          <Alert severity="success">{successMsg}</Alert>
        </Snackbar>
      ) : (
        <></>
      )}
    </>
  );
};

export default SingleProduct;
