import React, { useEffect } from "react";
import ProductCard from "./ProductCard";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { useDispatch, useSelector } from "react-redux";
import { loadAllProducts, productSelector } from "../slices/products";

const Content = () => {
  const dispatch = useDispatch();
  const { allProducts } = useSelector(productSelector);

  useEffect(() => {
    dispatch(loadAllProducts());
  }, []);

  return (
    <>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container rowSpacing={6} columnSpacing={4}>
          {Object.keys(allProducts).length > 0 ? (
            allProducts.map((product) => {
              return (
                <Grid item xs={3} key={product._id}>
                  <ProductCard key={product._id} product={product} />
                </Grid>
              );
            })
          ) : (
            <></>
          )}
        </Grid>
      </Box>
    </>
  );
};

export default Content;
