import axios from "axios";
import React from "react";
import { useNavigate } from "react-router-dom";
import InvoiceStripe from "../Components/Stripe/InvoiceStripe";

const SuccessPageStripe = () => {
  const Navigate = useNavigate();
  const emptyCart = () => {
    var data = JSON.stringify({
      user_ID: localStorage.getItem("user_id"),
    });
    var config = {
      method: "delete",
      // for express
      url: "http://localhost:8080/empty_cart",
      // for moleculer
      // url: "http://localhost:5000/api/user/empty_cart",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };
    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        localStorage.removeItem("PI");
        Navigate("/homepage");
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <>
      <h2 style={{ textAlign: "center" }}>Thanks For Shopping...</h2>
      <InvoiceStripe />
      <br />
      <div style={{ textAlign: "center", marginBottom: "2rem" }}>
        <p style={{ color: "blue", cursor: "pointer" }} onClick={emptyCart}>
          <span style={{ color: "black" }}>Get back to </span> Home
        </p>
      </div>
    </>
  );
};

export default SuccessPageStripe;
