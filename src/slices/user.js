import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
  current_user: {},
  load: false,
  allUsers: {},
};

const userSlice = createSlice({
  name: "users",
  initialState,
  reducers: {
    addCurrentUser: (state, { payload }) => {
      state.current_user = payload;
      state.load = false;
    },
    loadCurrentUser: (state) => {
      state.load = true;
    },
    addAllUsers: (state, { payload }) => {
      state.allUsers = payload;
      state.load = false;
    },
    loadAllUsers: (state) => {
      state.load = true;
    },
  },
});

export const { addCurrentUser, loadCurrentUser, addAllUsers, loadAllUsers } =
  userSlice.actions;

export const userSelector = (state) => state.user;

export default userSlice.reducer;
