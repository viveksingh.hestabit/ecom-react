import { all } from "redux-saga/effects";
import watchChatMessage from "./chatMessageSaga";
import watchAllProducts from "./productSaga";
import watchCurrentUser from "./userSaga";

export default function* rootSaga() {
  yield all([watchCurrentUser(), watchChatMessage(), watchAllProducts()]);
}
