import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import ItemCard from "../Components/ItemCard";
import Navbar from "../Components/Navbar";
import TotalAmountAtCart from "../Components/TotalAmountAtCart";
import { loadCurrentUser, userSelector } from "../slices/user";
import style from "../Styles/cartpage.module.css";

const CartPage = () => {
  const dispatch = useDispatch();
  const { current_user } = useSelector(userSelector);

  useEffect(() => {
    dispatch(loadCurrentUser());
  }, [dispatch]);

  return (
    <>
      <Navbar />
      <div className={`${style.cartpage_container}`}>
        <div className={`${style.extra_div}`}></div>
        <div className={`${style.divs_holder}`}>
          <div className={`${style.addedProductContainer}`}>
            <div className={`${style.heading_holder}`}>
              <h1 className={`${style.myCart_heading}`}>MY CART</h1>
            </div>
            {current_user.cart?.length === 0 ? (
              <h4 style={{ textAlign: "center" }}>Your Cart is empty!</h4>
            ) : (
              ""
            )}
            {current_user.cart?.map((cartProduct, idx) => {
              return <ItemCard cartProduct={cartProduct} key={idx} />;
            })}
          </div>
          <div className={`${style.totalAmountContainer}`}>
            {current_user.cart === undefined ? "" : <TotalAmountAtCart />}
          </div>
        </div>
      </div>
    </>
  );
};

export default CartPage;
