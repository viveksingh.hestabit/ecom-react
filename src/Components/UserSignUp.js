import React, { useState } from "react";
import style from "../Styles/login.module.css";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import { Alert, Button, Snackbar, TextField } from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import FileUploadOutlinedIcon from "@mui/icons-material/FileUploadOutlined";
import axios from "axios";

const UserSignUp = () => {
  const Navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [ID, setID] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [error, setError] = useState();
  const [imgUrl, setImgUrl] = useState("");
  const [open, setOpen] = useState(false);

  const addNewUser = (event) => {
    event.preventDefault();
    if (
      name !== "" &&
      email !== "" &&
      password !== "" &&
      confirmPassword !== "" &&
      ID !== ""
    ) {
      var data = JSON.stringify({
        name: name,
        email: email,
        ID: ID,
        img_url: imgUrl,
        password: password,
        confirmPassword: confirmPassword,
      });
      var config = {
        method: "post",
        url: "http://localhost:8080/add_user",
        headers: {
          "Content-Type": "application/json",
        },
        data: data,
      };
      axios(config)
        .then(function (response) {
          // console.log(response.data);
          if (!response.data.success) {
            setError(response.data.message);
            setOpen(true);
          } else {
            Navigate("/user_login");
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      setError("please fill all the required fields");
      setOpen(true);
    }
  };

  const uploadImgWidget = window.cloudinary.createUploadWidget(
    {
      cloudName: "dil4odqfn",
      uploadPreset: "decorUser",
      cropping: true,
      folder: "user_img",
    },
    (error, result) => {
      if (!error && result && result.event === "success") {
        setImgUrl(result.info.secure_url);
        // console.log("Done! Here is the image info: ", result.info);
      }
    }
  );

  return (
    <>
      <div className={`${style.card_container_signup} ${style.card_container}`}>
        <h2 className={`${style.loginHeading}`}>SignUp</h2>
        <form onSubmit={addNewUser}>
          <Box sx={{ width: "100%" }}>
            <Grid container rowSpacing={4} columnSpacing={{ xs: 5 }}>
              <Grid item xs={6}>
                <TextField
                  id="outlined-name"
                  label="Name"
                  variant="outlined"
                  required={true}
                  onChange={(event) => {
                    setName(event.target.value);
                  }}
                  value={name}
                  className={`${style.inputFields}`}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="outlined-email"
                  label="Email"
                  variant="outlined"
                  required={true}
                  onChange={(event) => {
                    setEmail(event.target.value);
                  }}
                  value={email}
                  className={`${style.inputFields}`}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="outlined-password"
                  label="Password"
                  variant="outlined"
                  required={true}
                  type="password"
                  onChange={(event) => {
                    setPassword(event.target.value);
                  }}
                  value={password}
                  className={`${style.inputFields}`}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="outlined-c-password"
                  label="Confirm Password"
                  variant="outlined"
                  required={true}
                  type="password"
                  onChange={(event) => {
                    setConfirmPassword(event.target.value);
                  }}
                  value={confirmPassword}
                  className={`${style.inputFields}`}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="outlined-username"
                  label="Username"
                  variant="outlined"
                  required={true}
                  onChange={(event) => {
                    setID(event.target.value);
                  }}
                  value={ID}
                  className={`${style.inputFields}`}
                />
              </Grid>
              <Grid item xs={6}>
                <Button
                  variant="contained"
                  className={`${style.img_upload}`}
                  onClick={() => uploadImgWidget.open()}
                >
                  <span>Profile Picture </span>
                  <FileUploadOutlinedIcon style={{ marginLeft: "10px" }} />
                </Button>
              </Grid>
            </Grid>
          </Box>
          <div className={`${style.signup_button}`}>
            <Button
              type="submit"
              variant="contained"
              style={{ width: "10rem" }}
              onClick={addNewUser}
            >
              Sign Up
            </Button>
          </div>
        </form>
        <div className={`${style.linking_signup}`}>
          <span style={{ color: "#fff" }}>Already a user? </span>
          <Link style={{ color: "#00adff" }} to="/user_login">
            Login
          </Link>
        </div>
      </div>
      {error ? (
        <Snackbar
          open={open}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          autoHideDuration={3000}
          onClose={() => setOpen(false)}
        >
          <Alert severity="error">{error}!</Alert>
        </Snackbar>
      ) : (
        <></>
      )}
    </>
  );
};

export default UserSignUp;
