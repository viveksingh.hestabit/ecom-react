import React, { useContext, useEffect, useState } from "react";
import IconButton from "@mui/material/IconButton";
import Avatar from "@mui/material/Avatar";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import AttachFileIcon from "@mui/icons-material/AttachFile";
import SendIcon from "@mui/icons-material/Send";
import MessagesBox from "./MessagesBox";
import style from "../Styles/chatPage.module.css";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import axios from "axios";
import { loadChatMessage } from "../slices/chatMessage";
import { useDispatch, useSelector } from "react-redux";
import { userSelector } from "../slices/user";
import Lottie from "react-lottie";
import animationData from "../animations/typing.json";
import { SocketContext } from "../context/socket";

let selectedChatCompare;

const SelectedChatUser = (props) => {
  const socket = useContext(SocketContext);
  const dispatch = useDispatch();
  const [msg, setMsg] = useState("");
  const [chatMsg, setChatMsg] = useState([]);
  const [typing, setTyping] = useState(false);
  const [istyping, setIsTyping] = useState(false);

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  const { current_user } = useSelector(userSelector);

  const sendMedia = (url) => {
    let type;
    if (url.substring(url.length - 3, url.length) === "mp4") {
      type = "video";
    } else {
      type = "image";
    }
    var data = JSON.stringify({
      content: url,
      chatId: localStorage.getItem("chat_id"),
      sender_id: current_user._id,
      contentType: type,
    });

    var config = {
      method: "post",
      url: "http://localhost:8080/add_new_msg",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        // console.log(response.data);
        dispatch(loadChatMessage());
        socket.emit("new message", response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const sendMessageHandler = (event) => {
    event.preventDefault();
    if (msg !== "") {
      socket.emit("stop typing", props.selectedChat._id);
      var data = JSON.stringify({
        content: msg,
        chatId: localStorage.getItem("chat_id"),
        sender_id: current_user._id,
        contentType: "text",
      });

      var config = {
        method: "post",
        url: "http://localhost:8080/add_new_msg",
        headers: {
          "Content-Type": "application/json",
        },
        data: data,
      };

      axios(config)
        .then(function (response) {
          // console.log(JSON.stringify(response.data));
          setMsg("");
          dispatch(loadChatMessage());
          socket.emit("new message", response.data);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };

  const newMsgHandler = (event) => {
    setMsg(event.target.value);
    if (!props.socketConnected) return;

    if (!typing) {
      setTyping(true);
      socket.emit("typing", props.selectedChat._id);
    }
    let lastTypingTime = new Date().getTime();
    var timerLength = 3000;
    setTimeout(() => {
      var timeNow = new Date().getTime();
      var timeDiff = timeNow - lastTypingTime;
      if (timeDiff >= timerLength && typing) {
        socket.emit("stop typing", props.selectedChat._id);
        setTyping(false);
      }
    }, timerLength);
  };

  const uploadImgWidget = window.cloudinary.createUploadWidget(
    {
      cloudName: "dil4odqfn",
      uploadPreset: "decorimg_chat",
      cropping: true,
      folder: "chat_img",
    },
    (error, result) => {
      if (!error && result && result.event === "success") {
        sendMedia(result.info.secure_url);
        // console.log("Done! Here is the image info: ", result.info);
      }
    }
  );

  useEffect(() => {
    selectedChatCompare = props.selectedChat;
    socket.emit("join chat", props.selectedChat?._id);
  }, [props.selectedChat]);

  useEffect(() => {
    socket.on("typing", () => setIsTyping(true));
    socket.on("stop typing", () => setIsTyping(false));
  }, []);

  useEffect(() => {
    dispatch(loadChatMessage());
  }, [dispatch]);

  useEffect(() => {
    socket.on("message recieved", (newMessageRecieved) => {
      if (
        !selectedChatCompare ||
        selectedChatCompare._id !== newMessageRecieved.chat._id
      ) {
        // not using
      } else {
        setChatMsg([newMessageRecieved, ...chatMsg]);
      }
    });
  });

  return (
    <>
      {props.chattingUser ? (
        <div className={`${style.chatBox_chatInfo}`}>
          <div className={`${style.chatInfo_navbar}`}>
            <div className={`${style.user_details}`}>
              <Avatar
                alt="User"
                src={props.chattingUser?.img_url}
                className={`${style.user_avatar}`}
              />
              <h3 className={`${style.user_chatting_name}`}>
                {props.chattingUser?.name}
              </h3>
            </div>
            <IconButton
              type="button"
              sx={{ p: "10px" }}
              aria-label="chat_menu"
              className={`${style.option_hover}`}
            >
              <MoreVertIcon className={`${style.more_icon}`} />
            </IconButton>
          </div>
          <MessagesBox
            chatMsg={chatMsg}
            setChatMsg={setChatMsg}
            istyping={istyping}
          />
          {istyping ? (
            <div className={`${style.typingAnimation}`}>
              <Lottie
                options={defaultOptions}
                height={30}
                width={70}
                style={{
                  marginBottom: 0,
                  marginLeft: 0,
                  backgroundColor: "rgb(220,220,220)",
                }}
              />
            </div>
          ) : (
            <></>
          )}
          <div className={`${style.chatInfo_footer}`}>
            <Box
              component="form"
              sx={{
                "& > :not(style)": {
                  width: "100%",
                  backgroundColor: "rgb(236,236,236)",
                },
              }}
              style={{ width: "80%" }}
              autoComplete="off"
              noValidate
              onSubmit={sendMessageHandler}
            >
              <TextField
                id="outlined-basic"
                label="Message"
                variant="outlined"
                onChange={newMsgHandler}
                value={msg}
              />
            </Box>
            <IconButton
              type="button"
              sx={{ p: "10px" }}
              aria-label="attach"
              onClick={() => {
                uploadImgWidget.open();
              }}
            >
              <AttachFileIcon className={`${style.message_attachButton}`} />
            </IconButton>
            <IconButton
              type="button"
              sx={{ p: "10px" }}
              aria-label="send"
              onClick={sendMessageHandler}
            >
              <SendIcon className={`${style.message_sendButton}`} />
            </IconButton>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
};

export default SelectedChatUser;
