import React from "react";
import Navbar from "../Components/Navbar";
import SingleProduct from "../Components/SingleProduct";
import Sidebar from "../Components/Sidebar";
import style from "../Styles/homepage.module.css";

const SingleProductPage = () => {
  return (
    <>
      <Navbar />
      <div className={`${style.complete_container}`}>
        <div className={`${style.sidebar}`}>
          <Sidebar />
        </div>
        <div className={style.main_content}>
          <SingleProduct />
        </div>
      </div>
    </>
  );
};

export default SingleProductPage;
