import React, { useContext, useEffect, useState } from "react";
import style from "../Styles/chatPage.module.css";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import ChatUsers from "./ChatUsers";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import { loadChatMessage } from "../slices/chatMessage";
import SelectedChatUser from "./SelectedChatUser";
import { loadAllUsers, userSelector } from "../slices/user";
import { SocketContext } from "../context/socket";

const ChatBox = () => {
  const dispatch = useDispatch();
  const socket = useContext(SocketContext);

  const { current_user, allUsers } = useSelector(userSelector);
  const [chattingUser, setChattingUser] = useState(null);
  const [searchResult, setSearchResult] = useState();
  const [searchValue, setSearchValue] = useState("");
  const [selectedChat, setSelectedChat] = useState();
  const [socketConnected, setSocketConnected] = useState(false);

  const addChat = (reciever_id) => {
    var data = JSON.stringify({
      loggedInUserId: current_user._id,
      reciverUserId: reciever_id,
    });

    var config = {
      method: "post",
      url: "http://localhost:8080/add_new_chat",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        setSelectedChat(response.data);
        localStorage.setItem("chat_id", response.data._id);
        setTimeout(() => {
          dispatch(loadChatMessage());
        }, 600);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const handleUserSearch = (value) => {
    var logInUserId = localStorage.getItem("user_id");

    var config = {
      method: "get",
      url: `http://localhost:8080/search_user/${logInUserId}?search=${value}`,
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        setSearchResult(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const getChattingUser = (id) => {
    var config = {
      method: "get",
      url: `http://localhost:8080/get_chat_user/${id}`,
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        setChattingUser(response.data);
        setSearchResult([]);
        setSearchValue("");
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const changeStatusToOffline = () => {
    var data = JSON.stringify({
      id: localStorage.getItem("user_id"),
    });

    var config = {
      method: "post",
      url: "http://localhost:8080/logout_chat_user",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        socket.emit("offline");
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    dispatch(loadAllUsers());
    socket.emit("setup", localStorage.getItem("loginId"));
    socket.on("connected", () => setSocketConnected(true));
    return () => {
      changeStatusToOffline();
    };
  }, []);

  useEffect(() => {
    socket.on("online", () => {
      dispatch(loadAllUsers());
    });
    socket.on("offline", () => {
      dispatch(loadAllUsers());
    });
  }, [socket]);

  return (
    <>
      <div className={`${style.chatBox_container}`}>
        <div className={`${style.chatBox}`}>
          <div className={`${style.chatBox_sidebar}`}>
            <div className={`${style.searchUser}`}>
              <Box
                component="form"
                sx={{
                  "& > :not(style)": {
                    width: "100%",
                    backgroundColor: "rgb(236,236,236)",
                  },
                }}
                autoComplete="off"
                noValidate
                onSubmit={(event) => {
                  event.preventDefault();
                }}
              >
                <TextField
                  id="outlined-basic"
                  label="Search here"
                  variant="outlined"
                  onChange={(event) => {
                    setSearchValue(event.target.value);
                    handleUserSearch(event.target.value);
                  }}
                  value={searchValue}
                />
              </Box>
            </div>
            <div className={`${style.all_chat_users}`}>
              {searchValue !== "" ? (
                searchResult?.map((item) => (
                  <ChatUsers
                    item={item}
                    getChattingUser={getChattingUser}
                    addChat={addChat}
                    key={item._id}
                  />
                ))
              ) : Object.keys(allUsers).length === 0 ? (
                <></>
              ) : (
                allUsers.map((item) => (
                  <ChatUsers
                    item={item}
                    getChattingUser={getChattingUser}
                    addChat={addChat}
                    key={item._id}
                  />
                ))
              )}
            </div>
          </div>
          {chattingUser ? (
            <SelectedChatUser
              chattingUser={chattingUser}
              selectedChat={selectedChat}
              socketConnected={socketConnected}
            />
          ) : (
            ""
          )}
        </div>
      </div>
    </>
  );
};

export default ChatBox;
