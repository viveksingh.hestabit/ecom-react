import { call, put, takeEvery } from "redux-saga/effects";
import { addAllMsgOfAChatToRedux } from "../API/chatMessagesApi";
import { getChatMessages } from "../slices/chatMessage";

function* workChatMessage() {
  const data = yield call(() => {
    return addAllMsgOfAChatToRedux();
  });
  yield put(getChatMessages(data));
}

function* watchChatMessage() {
  yield takeEvery("chatMessages/loadChatMessage", workChatMessage);
}

export default watchChatMessage;
