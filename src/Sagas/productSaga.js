import { call, put, takeEvery } from "redux-saga/effects";
import { getAllProducts } from "../API/productsAPI";
import { addAllProducts } from "../slices/products";

function* workAddAllProducts() {
  const data = yield call(() => {
    return getAllProducts();
  });
  yield put(addAllProducts(data));
}

function* watchAllProducts() {
  yield takeEvery("products/loadAllProducts", workAddAllProducts);
}

export default watchAllProducts;
