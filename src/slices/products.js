import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
  allProducts: {},
  load: false,
};

const productSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    addAllProducts: (state, { payload }) => {
      state.allProducts = payload;
      state.load = false;
    },
    loadAllProducts: (state) => {
      state.load = true;
    },
  },
});

export const { addAllProducts, loadAllProducts } = productSlice.actions;

export const productSelector = (state) => state.product;

export default productSlice.reducer;
