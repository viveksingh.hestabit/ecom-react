import React from "react";
import CircleIcon from "@mui/icons-material/Circle";
import style from "../Styles/chatPage.module.css";
import Avatar from "@mui/material/Avatar";

const ChatUsers = (props) => {
  return (
    <>
      <div
        className={`${style.chat_user}`}
        key={props.item._id}
        onClick={() => {
          props.getChattingUser(props.item._id);
          props.addChat(props.item._id);
        }}
      >
        <div className={`${style.chat_user_info}`}>
          <Avatar
            alt="User"
            src={props.item.img_url}
            className={`${style.chat_user_avatar}`}
          />
          <h4 className={`${style.chat_user_name}`}>{props.item.name}</h4>
        </div>
        <CircleIcon
          className={
            props.item.status
              ? `${style.chat_user_status_online}`
              : `${style.chat_user_status_offline}`
          }
        />
      </div>
    </>
  );
};

export default ChatUsers;
