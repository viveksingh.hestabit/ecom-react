import React, { useState } from "react";
import style from "../Styles/itemcard.module.css";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import { useSelector } from "react-redux";
import { userSelector } from "../slices/user";
import CheckoutModel from "./CheckoutModel";
import { useEffect } from "react";

const styleModal = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const TotalAmountAtCart = () => {
  const { current_user } = useSelector(userSelector);

  const [paypalButtons, setPaypalButtons] = useState(false);
  const [open, setOpen] = useState(false);
  const [totalPrice, setTotalPrice] = useState(false);
  const [subTotal, setSubTotal] = useState(false);
  const [tax, setTax] = useState(false);
  const [deliveryCharge, setDeliveryCharge] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setOpen(false);
    setPaypalButtons(false);
  };

  useEffect(() => {
    const total = current_user.cart.reduce(
      (a, b) => Number(a) + Number(b.price) * b.quantity,
      0
    );
    const sub = total - (18 * total) / 100;
    const taxes = (total - sub).toFixed(2);
    const delivery = sub > 0 ? 100 : 0;
    setTotalPrice(total);
    setSubTotal(sub);
    setTax(taxes);
    setDeliveryCharge(delivery);
  }, [current_user]);

  return (
    <>
      <div className={`${style.totalAmount_heading_container}`}>
        <h1 className={`${style.totalAmount_heading}`}>Total</h1>
      </div>
      <div className={`${style.totalAmount_subInfo_container}`}>
        <div className={`${style.totalAmount_charges_container}`}>
          <h3 className={`${style.totalAmount_charge_name}`}>Sub Total</h3>
          <p>{subTotal}</p>
        </div>
        <div className={`${style.totalAmount_charges_container}`}>
          <h3 className={`${style.totalAmount_charge_name}`}>Taxes</h3>
          <p>{tax}</p>
        </div>
        <div className={`${style.totalAmount_charges_container}`}>
          <h3 className={`${style.totalAmount_charge_name}`}>Shipping</h3>
          <p>{deliveryCharge}</p>
        </div>
      </div>
      <div className={`${style.totalAmount_charges_container}`}>
        <h3 className={`${style.totalAmount_Total}`}>Total Amount</h3>
        <p>{totalPrice + deliveryCharge}</p>
      </div>
      {deliveryCharge === 0 ? (
        <Button className={`${style.checkout_button_disabled}`} disabled>
          Checkout
        </Button>
      ) : (
        <Button className={`${style.checkout_button}`} onClick={handleOpen}>
          Checkout
        </Button>
      )}
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={styleModal}>
          <CheckoutModel
            setPaypalButtons={setPaypalButtons}
            paypalButtons={paypalButtons}
            totalPrice={totalPrice}
            deliveryCharge={deliveryCharge}
          />
        </Box>
      </Modal>
    </>
  );
};

export default TotalAmountAtCart;
