import React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import style from "../Styles/invoice.module.css";
import { useSelector } from "react-redux";
import { userSelector } from "../slices/user";

const InvoiceDataTable = () => {
  const { current_user } = useSelector(userSelector);
  return (
    <>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead className={`${style.heading_table}`}>
            <TableRow>
              <TableCell className={`${style.table_head_text}`}>
                Items
              </TableCell>
              <TableCell align="right" className={`${style.table_head_text}`}>
                Description
              </TableCell>
              <TableCell align="right" className={`${style.table_head_text}`}>
                Unit Cost
              </TableCell>
              <TableCell align="right" className={`${style.table_head_text}`}>
                Quantity
              </TableCell>
              <TableCell align="right" className={`${style.table_head_text}`}>
                Line Total
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {current_user.cart?.map((row) => (
              <TableRow
                key={row.name}
                sx={{
                  "&:last-child td, &:last-child th": { border: 0 },
                }}
              >
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="right">{row.description}</TableCell>
                <TableCell align="right">{row.price}</TableCell>
                <TableCell align="right">{row.quantity}</TableCell>
                <TableCell align="right">{row.price * row.quantity}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default InvoiceDataTable;
