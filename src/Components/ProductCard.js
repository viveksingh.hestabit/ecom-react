import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import style from "../Styles/product.module.css";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { loadCurrentUser, userSelector } from "../slices/user";
import { useNavigate } from "react-router-dom";
import { Alert, Snackbar } from "@mui/material";

const ProductCard = (props) => {
  const Navigate = useNavigate();
  const dispatch = useDispatch();
  const { current_user } = useSelector(userSelector);
  const [logError, setLogError] = useState();
  const [open, setOpen] = useState(false);
  const [successMsg, setSuccessMsg] = useState();
  const [successOpen, setSuccessOpen] = useState(false);

  const addToCart = (ID) => {
    var data = JSON.stringify({
      user_ID: {
        _id: current_user._id,
      },
      product: {
        product_id: ID,
        price: props.product.price,
        name: props.product.name,
        description: props.product.description,
        img_url: props.product.prod_img_url,
        quantity: 1,
        stock: props.product.stock,
      },
    });
    var config = {
      method: "post",
      // for express
      url: "http://localhost:8080/add_to_cart",
      // for moleculer
      // url: "http://localhost:5000/api/user/product/add_to_cart",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };
    axios(config)
      .then(function (res) {
        // console.log(JSON.stringify(response.data));
        if (res.data.success) {
          dispatch(loadCurrentUser());
          setSuccessOpen(true);
          setSuccessMsg(res.data.message);
        } else {
          setLogError(res.data.message);
          setOpen(true);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <>
      <Card
        sx={{ maxWidth: 255, maxHeight: 340 }}
        className={`${style.product_card}`}
      >
        <CardMedia
          component="img"
          alt="product"
          height="180"
          image={props.product.prod_img_url}
          style={{ cursor: "pointer" }}
          onClick={() => Navigate(`/products/${props.product._id}`)}
        />
        <CardContent
          onClick={() => Navigate(`/products/${props.product._id}`)}
          style={{ cursor: "pointer" }}
        >
          <Typography
            gutterBottom
            variant="h5"
            component="div"
            className={`${style.product_title}`}
          >
            {props.product.name}
          </Typography>
          <Typography
            variant="body2"
            color="text.secondary"
            className={`${style.product_description}`}
          >
            {props.product.description}
          </Typography>
          {logError ? (
            <>
              <Snackbar
                open={open}
                autoHideDuration={2000}
                onClose={() => setOpen(false)}
              >
                <Alert severity="error">{logError}!</Alert>
              </Snackbar>
            </>
          ) : (
            <></>
          )}
        </CardContent>
        <CardActions>
          <div className={`${style.price_and_button}`}>
            <h3 className={`${style.price}`}>&#8377; {props.product.price}</h3>
            <Button
              size="medium"
              className={`${style.addToCart_button}`}
              onClick={() => {
                addToCart(props.product._id);
              }}
            >
              Add To Cart
            </Button>
          </div>
        </CardActions>
      </Card>
      {successMsg ? (
        <Snackbar
          open={successOpen}
          autoHideDuration={2000}
          onClose={() => setSuccessOpen(false)}
        >
          <Alert severity="success">{successMsg}</Alert>
        </Snackbar>
      ) : (
        <></>
      )}
    </>
  );
};

export default ProductCard;
