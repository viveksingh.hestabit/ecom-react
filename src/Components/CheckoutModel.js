import { Button } from "@mui/material";
import axios from "axios";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { userSelector } from "../slices/user";
import style from "../Styles/itemcard.module.css";
import PaypalIntegration from "./Paypal/PaypalIntegration";
import RazorpayIntegration from "./Razorpay/RazorpayIntegration";
import StripeIntegration from "./Stripe/StripeIntegration";

const CheckoutModel = (props) => {
  const { current_user } = useSelector(userSelector);
  const [paypalItems, setPaypalItems] = useState({});

  const handleCheckoutPaypal = () => {
    props.setPaypalButtons(true);
    var data = JSON.stringify({
      prod: current_user.cart,
    });
    var config = {
      method: "post",
      // for express
      // url: "http://localhost:8080/generate_paypal_checkout",
      // for moleculer
      url: "http://localhost:5000/api/paypal/payment/create_checkout",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        setPaypalItems(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <>
      <h2 className={`${style.modalTitle}`}>Choose your payment option</h2>
      {props.paypalButtons === false ? (
        <>
          <StripeIntegration />
          <Button
            className={`${style.stripeButton}`}
            onClick={handleCheckoutPaypal}
          >
            Paypal
          </Button>
          <RazorpayIntegration
            totalPrice={props.totalPrice}
            deliveryCharge={props.deliveryCharge}
          />
        </>
      ) : (
        <PaypalIntegration paypalItems={paypalItems} />
      )}
    </>
  );
};

export default CheckoutModel;
