import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
  all_messages: [],
  load: true,
};

const chatMessageSlice = createSlice({
  name: "chatMessages",
  initialState,
  reducers: {
    getChatMessages: (state, { payload }) => {
      state.all_messages = payload;
      state.load = false;
    },
    loadChatMessage: (state) => {
      state.load = true;
    },
  },
});

export const { getChatMessages, loadChatMessage } = chatMessageSlice.actions;

export const chatMessageSelector = (state) => state.chatMessage;

export default chatMessageSlice.reducer;
