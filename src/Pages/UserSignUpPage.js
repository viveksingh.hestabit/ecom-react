import React from "react";
import UserSignUp from "../Components/UserSignUp";

const UserSignUpPage = () => {
  return (
    <>
      <div className="backgroundImg">
        <UserSignUp />
      </div>
    </>
  );
};

export default UserSignUpPage;
