import { call, put, takeEvery } from "redux-saga/effects";
import { addAllUsersToRedux, addCurrentUserToRedux } from "../API/usersAPI";
import { addAllUsers, addCurrentUser } from "../slices/user";

function* workCurrentUser() {
  const data = yield call(() => {
    return addCurrentUserToRedux();
  });
  yield put(addCurrentUser(data));
}

function* workAddAllUser() {
  const data = yield call(() => {
    return addAllUsersToRedux();
  });
  yield put(addAllUsers(data));
}

function* watchCurrentUser() {
  yield takeEvery("users/loadCurrentUser", workCurrentUser);
  yield takeEvery("users/loadAllUsers", workAddAllUser);
}

export default watchCurrentUser;
