import React, { useEffect } from "react";
import style from "../Styles/navbar.module.css";
import website_logo from "../website_logo.png";
import ShoppingCartOutlinedIcon from "@mui/icons-material/ShoppingCartOutlined";
import Avatar from "@mui/material/Avatar";
import { loadCurrentUser, userSelector } from "../slices/user";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Badge from "@mui/material/Badge";
import { IconButton } from "@mui/material";

const Navbar = () => {
  const dispatch = useDispatch();
  const Navigate = useNavigate();
  const { current_user } = useSelector(userSelector);

  useEffect(() => {
    dispatch(loadCurrentUser());
  }, [dispatch]);

  return (
    <>
      <div className={`${style.main_navbar}`}>
        <div className={`${style.left_nav}`}>
          <img
            src={website_logo}
            alt="logo"
            className={`${style.logo}`}
            onClick={() => Navigate("/homepage")}
          />
          <h1 className={`${style.website_name}`}>Let'z Decor</h1>
        </div>
        <div className={`${style.right_nav}`}>
          {current_user.cart === undefined ? (
            " "
          ) : (
            <IconButton
              type="button"
              sx={{ p: "10px" }}
              aria-label="cart"
              className={`${style.cart_icon_hover}`}
              onClick={() => {
                Navigate("/cart");
              }}
            >
              <Badge badgeContent={current_user.cart.length} color="error">
                <ShoppingCartOutlinedIcon className={`${style.cart_icon}`} />
              </Badge>
            </IconButton>
          )}
          <div className={`${style.user_detail}`}>
            <h3 className={`${style.loggedInUserName}`}>{current_user.name}</h3>
            <Avatar
              alt="profile"
              src={current_user.img_url}
              sx={{ width: 40, height: 40 }}
              referrerPolicy="no-referrer"
            />
            {/* <img src={current_user.img_url} alt="" referrerpolicy="no-referrer"/> if not getting avatar image */}
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
