import React from "react";
import { useNavigate } from "react-router-dom";
import style from "../Styles/sidebar.module.css";
import LogoutButton from "./LogoutButton";

const Sidebar = () => {
  const Navigate = useNavigate();
  const sidebarItems = ["Home", "My Cart", "Conversations"];

  const HandleNavigation = (key) => {
    if (key === 0) {
      Navigate("/homepage");
    } else if (key === 1) {
      Navigate("/cart");
    } else if (key === 2) {
      Navigate("/conversations");
    } else {
      Navigate("/homepage");
    }
  };

  return (
    <>
      <div className={`${style.all_links}`}>
        {sidebarItems.map((item, idx) => {
          return (
            <div
              className={`${style.list_items}`}
              onClick={() => {
                HandleNavigation(idx);
              }}
              key={idx}
            >
              {item}
            </div>
          );
        })}
        <LogoutButton />
      </div>
    </>
  );
};

export default Sidebar;
