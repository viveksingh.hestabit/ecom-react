import { Button } from "@mui/material";
import axios from "axios";
import React from "react";
import { useSelector } from "react-redux";
import { userSelector } from "../../slices/user";
import style from "../../Styles/itemcard.module.css";

const StripeIntegration = () => {
  const { current_user } = useSelector(userSelector);

  const handleCheckoutStripe = () => {
    var data = JSON.stringify({
      prod: current_user.cart,
    });
    var config = {
      method: "post",
      // for express
      url: "http://localhost:8080/create_checkout_session",
      // for moleculer
      // url:"http://localhost:5000/api/stripe/payment/create_checkout_session",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        if (response.data.id) {
          window.location.href = response.data.url;
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <>
      <Button
        className={`${style.stripeButton}`}
        onClick={handleCheckoutStripe}
      >
        Stripe
      </Button>
    </>
  );
};

export default StripeIntegration;
