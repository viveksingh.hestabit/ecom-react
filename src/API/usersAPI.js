import axios from "axios";

export const addCurrentUserToRedux = async () => {
  const response = await axios.get(
    // for express
    `http://localhost:8080/user/${localStorage.getItem("user_id")}`
    // for moleculer
    // `http://localhost:5000/api/user/current_user/${localStorage.getItem(
    //   "user_id"
    // )}`
  );
  localStorage.setItem("loginId", response.data._id);
  return response.data;
};

export const addAllUsersToRedux = async () => {
  const response = await axios.get(
    // for express
    `http://localhost:8080/users`
    // for moleculer
    // `http://localhost:5000/`
  );
  const data = await response.data.filter((item) => {
    return item.ID !== localStorage.getItem("user_id");
  });
  return data;
};
