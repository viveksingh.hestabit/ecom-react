import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";
import userSlice from "./slices/user";
import createSagaMiddleware from "@redux-saga/core";
import rootSaga from "./Sagas/rootSaga";
import chatMessageSlice from "./slices/chatMessage";
import productSlice from "./slices/products";

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
  reducer: {
    user: userSlice,
    chatMessage: chatMessageSlice,
    product: productSlice,
  },

  middleware: [sagaMiddleware],
});

sagaMiddleware.run(rootSaga);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  // <React.StrictMode>
  <Provider store={store}>
    <App />
  </Provider>
  // </React.StrictMode>
);
