import axios from "axios";

export const getAllProducts = async () => {
  // for express
  const url = "http://localhost:8080/products";
  // for moleculer
  // url: "http://localhost:5000/api/product/all_products",

  const response = await axios.get(url);
  return response.data;
};
