import React from "react";
import UserLogin from "../Components/UserLogin";

const UserLoginPage = () => {
  return (
    <>
      <div className="backgroundImg">
        <UserLogin />
      </div>
    </>
  );
};

export default UserLoginPage;
