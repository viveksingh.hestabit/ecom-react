import React from "react";
import ReactDOM from "react-dom";
import { useNavigate } from "react-router-dom";
const PayPalButton = window.paypal.Buttons.driver("react", { React, ReactDOM });

const PaypalIntegration = (props) => {
  const Navigate = useNavigate();
  const createOrder = (data, actions) => {
    return actions.order.create({
      purchase_units: [
        {
          amount: {
            currency_code: "USD",
            value: props.paypalItems.totalPrice.toString(),
            breakdown: {
              item_total: {
                currency_code: "USD",
                value: props.paypalItems.totalPrice.toString(),
              },
            },
          },
          items: props.paypalItems.itemArr,
          shipping: {
            options: [
              {
                id: "SHIP_123",
                label: "Shipping Charges",
                type: "SHIPPING",
                selected: true,
                amount: {
                  value: "10",
                  currency_code: "USD",
                },
              },
            ],
          },
        },
      ],
    });
  };
  const onApprove = (data, actions) => {
    return actions.order.capture().then(function (orderData) {
      Navigate("/success/paypal");
      // console.log(
      //   "Capture result",
      //   orderData
      // );
    });
  };
  return (
    <PayPalButton
      createOrder={(data, actions) => createOrder(data, actions)}
      onApprove={(data, actions) => onApprove(data, actions)}
    />
  );
};

export default PaypalIntegration;
