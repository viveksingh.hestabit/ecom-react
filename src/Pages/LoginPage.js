import React from "react";
import LoginCard from "../Components/LoginCard";

const LoginPage = () => {
  return (
    <>
      <div className="backgroundImg">
        <LoginCard />
      </div>
    </>
  );
};

export default LoginPage;
