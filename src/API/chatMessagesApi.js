import axios from "axios";

export async function addAllMsgOfAChatToRedux() {
  if (localStorage.getItem("chat_id")) {
    const { data } = await axios.get(
      // for express
      `http://localhost:8080/chat/${localStorage.getItem("chat_id")}`
      // for moleculer
      // `http://localhost:5000/`
    );
    // console.log(data);
    if (data.name !== "CastError") {
      var result = data?.reverse();
    }
    return result;
  } else {
    return [];
  }
}
