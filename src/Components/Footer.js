import { Button } from "@mui/material";
import React from "react";
import style from "../Styles/footer.module.css";
import EmailOutlinedIcon from "@mui/icons-material/EmailOutlined";
import PhoneIcon from "@mui/icons-material/Phone";
import ContactSupportIcon from "@mui/icons-material/ContactSupport";
import facebook from "../facebook.png";
import instagram from "../instagram.png";
import twitter from "../twitter.png";
import footer_logo from "../footer_logo.png";

const Footer = () => {
  return (
    <>
      <div className={`${style.footer_container}`}>
        <div className={`${style.footer_top}`}>
          <div className={`${style.holder}`}>
            <h2 className={`${style.heading}`}>Subscribe to us</h2>
            <p className={`${style.heading_desc}`}>
              Get our latest offers and news right in your inbox.
            </p>
            <div className={`${style.button_container}`}>
              <input type="text" className={`${style.input_email}`} />
              <Button
                variant="contained"
                className={`${style.subscribe_button}`}
              >
                Subscribe
              </Button>
            </div>
          </div>
          <div className={`${style.holder}`}>
            <h2 className={`${style.heading}`}>Enjoy our amazing apps</h2>
            <p className={`${style.heading_desc}`}>
              Shop our products and offers on-the-go.
            </p>
            <div
              className={`${style.button_container} ${style.download_button_container}`}
            >
              <img
                src="https://i1.lmsin.net/website_images/misc/apple-app-store.svg"
                alt="apple_icon"
                style={{ width: "160px" }}
              />

              <img
                src="https://i1.lmsin.net/website_images/misc/google-play-store.svg"
                alt="google_icon"
                style={{ width: "160px" }}
              />
            </div>
          </div>
        </div>
        <div className={`${style.footer_bottom}`}>
          <div className={`${style.communication}`}>
            <div className={`${style.icon_info_holder}`}>
              <PhoneIcon style={{ color: "#fff", fontSize: "37px" }} />
              <div style={{ marginLeft: "0.5rem" }}>
                <p className={`${style.heading_desc}`}>Talk to us</p>
                <p className={`${style.heading} ${style.extraInfo}`}>
                  1800-878-XXXX
                </p>
              </div>
            </div>
            <div className={`${style.icon_info_holder}`}>
              <ContactSupportIcon style={{ color: "#fff", fontSize: "37px" }} />
              <div style={{ marginLeft: "0.5rem" }}>
                <p className={`${style.heading_desc}`}>Help Centre</p>
                <p className={`${style.heading} ${style.extraInfo}`}>
                  letzdecor.in/help
                </p>
              </div>
            </div>
            <div className={`${style.icon_info_holder}`}>
              <EmailOutlinedIcon style={{ color: "#fff", fontSize: "37px" }} />
              <div style={{ marginLeft: "0.5rem" }}>
                <p className={`${style.heading_desc}`}>Write to us</p>
                <p className={`${style.heading} ${style.extraInfo}`}>
                  help@letzdecor.in
                </p>
              </div>
            </div>
          </div>
          <div className={`${style.social_media}`}>
            <img
              src={facebook}
              alt="fb_icon"
              className={`${style.social_media_icons}`}
            />
            <img
              src={twitter}
              alt="twitter_icon"
              className={`${style.social_media_icons}`}
            />
            <img
              src={instagram}
              alt="insta_icon"
              className={`${style.social_media_icons}`}
            />
          </div>
        </div>
        <div className={`${style.company_info_holder}`}>
          <div className={`${style.flexDiv}`}>
            <img
              src={footer_logo}
              alt="home_icon"
              className={`${style.home_icon}`}
            />
            <div className={`${style.adress_info}`}>
              <p className={`${style.address}`}>
                12, xyz street, industrial area
              </p>
              <p className={`${style.address}`}>Noida, UP-101025</p>
            </div>
          </div>
          <div className={`${style.anchors}`}>
            <a href="/" className={`${style.terms}`}>
              Terms and Conditions
            </a>
            <a href="/" className={`${style.terms}`}>
              Privacy Policy{" "}
            </a>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
