import { Button } from "@mui/material";
import axios from "axios";
import React from "react";
import { useNavigate } from "react-router-dom";
import style from "../../Styles/itemcard.module.css";

const RazorpayIntegration = (props) => {
  const Navigate = useNavigate();

  const initPayment = (data) => {
    const options = {
      key: "rzp_test_8o9WqTVN2VsjAw",
      amount: props.totalPrice,
      currency: "INR",
      name: "Letz Decor",
      order_id: data.id,
      handler: async (response) => {
        try {
          // for express
          const verifyUrl = "http://localhost:8080/api/payment/verify";
          // for moleculer
          // const verifyUrl = "http://localhost:5000/api/razorpay/payment/verify";
          const { data } = await axios.post(verifyUrl, response);
          // console.log(data);
          if (data.message === "Payment verified successfully") {
            Navigate("/success/razorpay");
          }
        } catch (err) {
          console.log(err);
        }
      },
      theme: {
        color: "#686CFD",
      },
    };
    const rzp1 = new window.Razorpay(options);
    rzp1.open();
  };

  const handleCheckoutRazorpay = async () => {
    try {
      // for express
      const orderUrl = "http://localhost:8080/api/payment/orders";
      // for moleculer
      // const orderUrl =
      //   "http://localhost:5000/api/razorpay/payment/create_order";
      const { data } = await axios.post(orderUrl, {
        amount: props.totalPrice + props.deliveryCharge,
      });
      initPayment(data.data);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <Button
        className={`${style.stripeButton}`}
        onClick={handleCheckoutRazorpay}
      >
        Razorpay
      </Button>
    </>
  );
};

export default RazorpayIntegration;
