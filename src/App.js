import "./App.css";
import LoginPage from "./Pages/LoginPage";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import HomePage from "./Pages/HomePage";
import CartPage from "./Pages/CartPage";
import SuccessPageStripe from "./Pages/SuccessPageStripe";
import SuccessPagePaypal from "./Pages/SuccessPagePaypal";
import SuccessPageRazorpay from "./Pages/SuccessPageRazorpay";
import ChatPage from "./Pages/ChatPage";
import { socket, SocketContext } from "./context/socket";
import UserLoginPage from "./Pages/UserLoginPage";
import UserSignUpPage from "./Pages/UserSignUpPage";
import SingleProductPage from "./Pages/SingleProductPage";

export const clientId =
  "666935426405-qvpcbk50b13qv1esf9rp4eocn8or5lcs.apps.googleusercontent.com";

function App() {
  return (
    <SocketContext.Provider value={socket}>
      <Router>
        <Routes>
          <Route
            path="/"
            element={
              <>
                <LoginPage />
              </>
            }
          />
          <Route
            path="/homepage"
            element={
              <>
                <HomePage />
              </>
            }
          />
          <Route
            path="/cart"
            element={
              <>
                <CartPage />
              </>
            }
          />
          <Route
            path="/success/stripe"
            element={
              <>
                <SuccessPageStripe />
              </>
            }
          />
          <Route
            path="/success/paypal"
            element={
              <>
                <SuccessPagePaypal />
              </>
            }
          />
          <Route
            path="/success/razorpay"
            element={
              <>
                <SuccessPageRazorpay />
              </>
            }
          />
          <Route
            path="/conversations"
            element={
              <>
                <ChatPage />
              </>
            }
          />
          <Route
            path="/user_login"
            element={
              <>
                <UserLoginPage />
              </>
            }
          />
          <Route
            path="/user_signup"
            element={
              <>
                <UserSignUpPage />
              </>
            }
          />
          <Route
            path="/products/:prodId"
            element={
              <>
                <SingleProductPage />
              </>
            }
          />
        </Routes>
      </Router>
    </SocketContext.Provider>
  );
}

export default App;
