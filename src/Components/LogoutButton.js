import React from "react";
import { GoogleLogout } from "react-google-login";
import { clientId } from "../App";
import { useNavigate } from "react-router-dom";
import style from "../Styles/sidebar.module.css";
import axios from "axios";

const LogoutButton = () => {
  const Navigate = useNavigate();

  const logOut = () => {
    localStorage.clear();
    Navigate("/");
  };

  const logoutOthers = () => {
    var data = JSON.stringify({
      id: localStorage.getItem("user_id"),
    });

    var config = {
      method: "post",
      url: "http://localhost:8080/logout_chat_user",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        logOut();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const handleFbLogout = () => {
    logoutOthers();
    window.FB.logout((res) => {});
  };

  return (
    <>
      {localStorage.getItem("google") === "true" ? (
        <GoogleLogout
          clientId={clientId}
          render={(renderProps) => (
            <button
              onClick={renderProps.onClick}
              disabled={renderProps.disabled}
              className={` ${style.list_items} ${style.logout_button}`}
            >
              Logout
            </button>
          )}
          onLogoutSuccess={logoutOthers}
        />
      ) : localStorage.getItem("facebook") === "true" ? (
        <button
          onClick={handleFbLogout}
          className={` ${style.list_items} ${style.logout_button}`}
        >
          Logout
        </button>
      ) : (
        <button
          onClick={logoutOthers}
          className={` ${style.list_items} ${style.logout_button}`}
        >
          Logout
        </button>
      )}
    </>
  );
};

export default LogoutButton;
