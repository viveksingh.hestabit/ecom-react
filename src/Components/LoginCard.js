import React, { useEffect } from "react";
import style from "../Styles/login.module.css";
import Stack from "@mui/material/Stack";
import { GoogleLogin } from "react-google-login";
import { gapi } from "gapi-script";
import { useNavigate } from "react-router-dom";
import { clientId } from "../App";
import FacebookLogin from "react-facebook-login";
import { Button } from "@mui/material";

const LoginCard = () => {
  const Navigate = useNavigate();

  const login = (id) => {
    Navigate("/homepage");
    localStorage.setItem("user_id", id);
  };

  const onSuccess = (res) => {
    // console.log(res.profileObj)
    localStorage.setItem("google", true);
    login(res.profileObj.googleId);
  };
  const onFailure = (err) => {
    console.log("failed:", err);
  };
  const facebookHandle = (res) => {
    if (res.status === "unknown") {
      console.log("error");
    } else {
      localStorage.setItem("facebook", true);
      login(res.id);
    }
  };

  useEffect(() => {
    const initClient = () => {
      gapi.auth2.getAuthInstance({
        clientId: clientId,
        scope: "",
      });
    };
    gapi.load("client:auth2", initClient);
  });
  return (
    <>
      <div className={`${style.card_container}`}>
        <Stack spacing={3} direction="column" width="100%">
          <FacebookLogin
            appId="496010529097335"
            autoLoad={false}
            fields="name,email,picture.width(150)"
            scope="public_profile,email,user_friends"
            callback={facebookHandle}
            icon="fa-facebook"
            size="small"
          />
          <GoogleLogin
            clientId={clientId}
            buttonText="Sign in with Google"
            onSuccess={onSuccess}
            onFailure={onFailure}
            cookiePolicy={"single_host_origin"}
            isSignedIn={true}
            className={`${style.google_button}`}
          />
          <div className={`${style.buttons}`}>
            <Button
              variant="contained"
              onClick={() => Navigate("/user_login")}
              style={{ width: "49%" }}
            >
              Login
            </Button>
            <Button
              variant="contained"
              onClick={() => Navigate("/user_signup")}
              style={{ width: "49%" }}
            >
              SignUp
            </Button>
          </div>
        </Stack>
      </div>
    </>
  );
};

export default LoginCard;
