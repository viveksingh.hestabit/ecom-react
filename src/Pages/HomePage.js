import React from "react";
import CarouselMain from "../Components/CarouselMain";
import Content from "../Components/Content";
import Footer from "../Components/Footer";
import Navbar from "../Components/Navbar";
import Sidebar from "../Components/Sidebar";
import style from "../Styles/homepage.module.css";

const HomePage = () => {
  return (
    <>
      <Navbar />
      <div className={`${style.complete_container}`}>
        <div className={`${style.sidebar}`}>
          <Sidebar />
        </div>
        <div className={style.main_content}>
          <div className={style.carousel}>
            <CarouselMain />
          </div>
          <div className={style.products}>
            <Content />
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default HomePage;
