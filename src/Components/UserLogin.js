import React, { useState } from "react";
import style from "../Styles/login.module.css";
import TextField from "@mui/material/TextField";
import axios from "axios";
import Stack from "@mui/material/Stack";
import { Link, useNavigate } from "react-router-dom";
import { Alert, Button, Snackbar } from "@mui/material";

const UserLogin = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState();
  const [open, setOpen] = useState();
  const Navigate = useNavigate();

  const handleLogin = (event) => {
    event.preventDefault();
    var data = JSON.stringify({
      email: email,
      password: password,
    });
    var config = {
      method: "post",
      url: "http://localhost:8080/login_chat_user",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };
    axios(config)
      .then(function (response) {
        // console.log(response.data);
        if (response.data.success) {
          Navigate("/homepage");
          localStorage.setItem("user_id", response.data.user.ID);
        } else {
          setError(response.data.message);
          setOpen(true);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <>
      <div className={`${style.card_container}`}>
        <h2 className={`${style.loginHeading}`}>Login</h2>
        <form onSubmit={handleLogin} className={`${style.loginForm}`}>
          <Stack spacing={3} direction="column" width="100%">
            <TextField
              id="outlined-basic"
              label="Email"
              variant="outlined"
              onChange={(event) => {
                setEmail(event.target.value);
              }}
              value={email}
              className={`${style.inputFields}`}
            />
            <TextField
              id="outlined-password-input"
              label="Password"
              type="password"
              autoComplete="current-password"
              onChange={(event) => {
                setPassword(event.target.value);
              }}
              value={password}
              className={`${style.inputFields}`}
            />
            <div style={{ textAlign: "center" }}>
              <Button
                type="submit"
                variant="contained"
                onClick={handleLogin}
                style={{ width: "10rem" }}
              >
                Login
              </Button>
            </div>
          </Stack>
        </form>
        <div className={`${style.linking}`}>
          <span style={{ color: "#fff" }}>Don't have an account? </span>
          <Link style={{ color: "#00adff" }} to="/user_signup">
            SignUp
          </Link>
        </div>
      </div>
      {error ? (
        <Snackbar
          open={open}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          autoHideDuration={3000}
          onClose={() => setOpen(false)}
        >
          <Alert severity="error">{error}!</Alert>
        </Snackbar>
      ) : (
        <></>
      )}
    </>
  );
};

export default UserLogin;
