import React, { useEffect, useState } from "react";
import style from "../../Styles/invoice.module.css";
import website_logo from "../../website_logo.png";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { loadCurrentUser, userSelector } from "../../slices/user";
import { Button } from "@mui/material";
import InvoiceDataTable from "../InvoiceDataTable";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import invoice from "../../invoice.pdf";

const Modalstyle = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const InvoiceToPdfStripe = (props) => {
  const dispatch = useDispatch();
  const [cust_details, setCust_Details] = useState([]);
  const [pay_Details, setPay_Details] = useState([]);
  const [amount_Details, setAmount_Details] = useState([]);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const { current_user } = useSelector(userSelector);

  const setCustomerDetails = (details) => {
    const arr = [
      details?.name,
      details?.address.line1,
      details?.address.line2,
      details?.address.city,
      details?.address.state,
      details?.address.postal_code,
      details?.email,
      details?.phone,
    ];

    setCust_Details(arr);
  };
  const setPaymentDetails = (details) => {
    const subTotal =
      details?.amount / 100 - 100 - 18 * ((details?.amount / 100 - 100) / 100);
    const tax = 18 * ((details?.amount / 100 - 100) / 100);
    const delivery = 100;
    const total = details?.amount_received / 100;

    const arr1 = [
      ["INVOICE", details?.id],
      ["Invoice Date", new Date(details?.created * 1000).toDateString()],
      ["Transaction Id", details?.latest_charge],
      [
        "Paid through",
        `${details?.payment_method_types}`,
      ],
    ];
    const arr2 = [
      ["SUB TOTAL", subTotal],
      ["TAX (18%)", tax],
      ["Delivery", delivery],
      ["TOTAL", total],
    ];
    setPay_Details(arr1);
    setAmount_Details(arr2);
  };

  const getCustomerDetails = (customer_id) => {
    var config = {
      method: "get",
      // for express
      url: `http://localhost:8080/stripe/customer/${customer_id}`,
      // for moleculer
      // url: `http://localhost:5000/api/stripe/payment/customer/${customer_id}`,
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        setCustomerDetails(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const getPaymentDetails = () => {
    var config = {
      method: "get",
      // for express
      url: "http://localhost:8080/stripe/invoice",
      // for moleculer
      // url: "http://localhost:5000/api/stripe/payment/invoice",
      headers: {},
    };

    axios(config)
      .then(function (response) {
        console.log(response.data);
        getCustomerDetails(response.data.customer);
        setPaymentDetails(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const downloadPdf = () => {
    var data = JSON.stringify({
      customerDetails: {
        name: cust_details[0],
        email: cust_details[6],
        address: {
          line1: cust_details[1],
          line2: cust_details[2],
          city: cust_details[3],
          state: cust_details[4],
          country: "India",
          postalCode: cust_details[5],
        },
        phoneNo: cust_details[7],
      },
      orderDetails: {
        invoiceId: pay_Details[0][1],
        invoiceDate: pay_Details[1][1],
        transactionId: pay_Details[2][1],
        paidThrough: pay_Details[3][1],
        products: current_user.cart,
        subTotal: amount_Details[0][1],
        tax: amount_Details[1][1],
        delivery: amount_Details[2][1],
        totalAmount: amount_Details[3][1],
      },
    });

    var config = {
      method: "post",
      // for express
      url: "http://localhost:8080/add_invoice",
      // for moleculer
      // url: "http://localhost:5000/api/invoice/add_invoice",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/pdf",
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
      })
      .catch(function (error) {
        console.log(error);
        alert("file already saved!");
      });
  };

  useEffect(() => {
    getPaymentDetails();
    dispatch(loadCurrentUser());
  }, [dispatch]);

  return (
    <>
      <div>
        <Button
          variant="contained"
          className={`${style.download_button}`}
          onClick={() => {
            downloadPdf();
            handleOpen();
          }}
        >
          Export
        </Button>

        <div className={`${style.invoice_container}`}>
          <div className={`${style.website_logo_name}`}>
            <img src={website_logo} alt="logo" className={`${style.logo}`} />
            <h1 className={`${style.website_name}`}>Let'z Decor</h1>
          </div>
          <div className={`${style.customer_invoice_details}`}>
            <div className={`${style.customer_details}`}>
              <h3 className={`${style.billto_heading}`}>BILL TO:</h3>
              {cust_details.map((customer, idx) => {
                return (
                  <p className={`${style.customer_info}`} key={idx}>
                    {customer}
                  </p>
                );
              })}
            </div>
            <div className={`${style.invoice_details}`}>
              {pay_Details.map((payment, idx) => {
                return (
                  <div className={`${style.invoice_key_value}`} key={idx}>
                    <p className={`${style.invoice_key}`}>{payment[0]}</p>
                    <p className={`${style.invoice_value}`}>{payment[1]}</p>
                  </div>
                );
              })}
            </div>
          </div>
          <div className={`${style.products}`}>
            <InvoiceDataTable />
          </div>
          <div className={`${style.totalPrice_tax_container}`}>
            <div className={`${style.totalPrice_tax}`}>
              {amount_Details.map((amount, idx) => {
                return (
                  <div className={`${style.invoice_key_value} `} key={idx}>
                    <p className={`${style.invoice_key}`}>{amount[0]}</p>
                    <p className={`${style.invoice_value}`}>
                      &#8377;{amount[1]}
                    </p>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
      <Modal open={open} onClose={handleClose}>
        <Box sx={Modalstyle}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Confirm Invoice Download..
          </Typography>
          <div className={`${style.modalButtons}`}>
            <Button variant="contained" onClick={handleClose}>
              Cancel
            </Button>
            <Button
              variant="contained"
              href={invoice}
              download="Invoice"
              onClick={handleClose}
            >
              Confirm
            </Button>
          </div>
        </Box>
      </Modal>
    </>
  );
};

export default InvoiceToPdfStripe;
