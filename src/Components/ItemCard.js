import React from "react";
import style from "../Styles/itemcard.module.css";
import CloseIcon from "@mui/icons-material/Close";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { loadCurrentUser, userSelector } from "../slices/user";
import { Button, IconButton } from "@mui/material";
import { useNavigate } from "react-router-dom";

const ItemCard = (props) => {
  const dispatch = useDispatch();
  const Navigate = useNavigate();
  const { current_user } = useSelector(userSelector);

  const removeItemFromCart = (prod_id) => {
    var data = JSON.stringify({
      user_ID: {
        _id: current_user._id,
      },
      prod_ID: {
        product_id: prod_id,
      },
    });
    var config = {
      method: "delete",
      // for express
      url: "http://localhost:8080/remove_from_cart",
      // for moleculer
      // url:"http://localhost:5000/api/user/product/remove_from_cart",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };
    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        dispatch(loadCurrentUser());
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const addQuantity = (prod_id) => {
    var data = JSON.stringify({
      userId: current_user._id,
      prodId: prod_id,
    });
    var config = {
      method: "post",
      url: "http://localhost:8080/add_quantity",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };
    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        if (response.data.success) {
          dispatch(loadCurrentUser());
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const subtractQuantity = (prod_id) => {
    var data = JSON.stringify({
      userId: current_user._id,
      prodId: prod_id,
    });
    var config = {
      method: "post",
      url: "http://localhost:8080/subtract_quantity",
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };
    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        dispatch(loadCurrentUser());
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <>
      <div className={`${style.itemCard_container}`}>
        <div className={`${style.itemcard_image}`}>
          <img
            src={props.cartProduct.img_url}
            alt="product"
            className={`${style.item_image}`}
            onClick={() => {
              Navigate(`/products/${props.cartProduct.product_id}`);
            }}
          />
        </div>
        <div className={`${style.itemcard_content}`}>
          <div className={`${style.name_and_cross}`}>
            <h1 className={`${style.item_name}`}>{props.cartProduct.name}</h1>
            <IconButton
              onClick={() => removeItemFromCart(props.cartProduct.product_id)}
            >
              <CloseIcon className={`${style.close_icon}`} />
            </IconButton>
          </div>
          <p className={`${style.item_description}`}>
            {props.cartProduct.description}
          </p>
          <div className={`${style.priceAndQuantity}`}>
            <div className={`${style.priceAndQuantity} ${style.quantity}`}>
              {props.cartProduct.quantity === 1 ? (
                <Button
                  variant="outlined"
                  className={`${style.quantity_button}`}
                  disabled
                >
                  -
                </Button>
              ) : (
                <Button
                  variant="outlined"
                  className={`${style.quantity_button}`}
                  onClick={() => subtractQuantity(props.cartProduct.product_id)}
                >
                  -
                </Button>
              )}

              <p style={{ fontSize: "larger" }}>{props.cartProduct.quantity}</p>
              <Button
                variant="outlined"
                className={`${style.quantity_button}`}
                onClick={() => addQuantity(props.cartProduct.product_id)}
              >
                +
              </Button>
            </div>
            <h2 className={`${style.item_price}`}>
              &#8377; {props.cartProduct.price}
            </h2>
          </div>
        </div>
      </div>
    </>
  );
};

export default ItemCard;
